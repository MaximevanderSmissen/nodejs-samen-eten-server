/* eslint-disable no-undef */
const chai = require("chai");
const expect = chai.expect;
const config = require("../../src/config/config");
const logger = config.logger;

const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const requester = require("../../requester");

//Models:
const Dorm = require("../../src/models/dorm.model");
const User = require("../../src/models/user.model");

//User service for token
const UserService = require("../../src/services/user.service");

const baseRoute = "/api/studenthome";

describe("In testing the Dorm router, ", function() {
	this.retries(1);

	let dormId;
	let storedUser;
	let authToken;

	const createErrorResponse = (properties, statusCode, errorMessage, done) => {
		Dorm.countDocuments().then(oldCount => {
			requester.post(baseRoute)
				.set("Authorization", `Bearer ${authToken}`)
				.send(properties)
				.end((_, res) => {
					expect(res).to.have.status(statusCode);
					expect(res.body).to.haveOwnProperty("error", errorMessage);
					expect(res.body).to.haveOwnProperty("date",config.errorDate());
					Dorm.countDocuments().then(newCount => {
						expect(newCount).to.equal(oldCount);
						done();
					});
				});
		});
	};

	beforeEach(done => {
		User.create({
			firstName: "FirstTestName",
			lastName: "LastTestName",
			studentNumber: "0",
			emailAddress: "test@test.nl",
			password: "TestPassowrd"
		}).then(user => {
			storedUser = user;
			authToken = UserService.signToken(user).token;

			return Dorm.create({
				name: "TestDorm1",
				maker: storedUser,
				streetName: "Street1",
				houseNumber: "1",
				postalCode: "1000AA",
				cityName: "City1",
				phoneNumber: "088 4950964"
			});
		}).then(newDorm => {
			dormId = newDorm._id;
			newDorm.meals.push({
				name: "Test Meal",
				maker: storedUser._id,
				offeredOn: new Date(2020, 3, 20),
				price: "0",
				allergyInfo: "Allergy Info",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 1
			});
			return newDorm.save({ validateBeforeSave: true });
		}).then(() => done())
			.catch(error => {
				logger.error("Something went wrong!");
				logger.error(error);
			});
	});

	describe("a POST to /api/studenthome ", () => {
		it("creates a new dorm", done => {
			const dormProps = {
				name: "PostCreationDorm",
				streetName: "TestStreet",
				houseNumber: "2",
				postalCode: "1000AA",
				cityName: "City1",
				phoneNumber: "088 4950964"
			};
	
			Dorm.countDocuments().then(oldCount => {
				requester.post(baseRoute)
					.set("Authorization", `Bearer ${authToken}`)
					.send(dormProps)
					.end((_, res) => {
						Dorm.findOne({ name: "PostCreationDorm" })
							.then(dorm => {
								expect(res).to.have.status(201);
								expect(res.body).to.haveOwnProperty("_id", dorm._id.toString());
								expect(res.body).to.haveOwnProperty("name", "PostCreationDorm");
								expect(res.body).to.haveOwnProperty("maker", dorm.maker.toString());
								expect(res.body).to.haveOwnProperty("streetName", "TestStreet");
								expect(res.body).to.haveOwnProperty("houseNumber", "2");
								expect(res.body).to.haveOwnProperty("postalCode", "1000AA");
								expect(res.body).to.haveOwnProperty("cityName", "City1");
								expect(res.body).to.haveOwnProperty("phoneNumber", "088 4950964");
								Dorm.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount + 1);
									done();
								});
							});
					});
			});
		});
	
		it("without name fails", done => {
			const dormProps = {
				streetName: "TestStreet",
				houseNumber: "2",
				postalCode: "1000AA",
				cityName: "City1",
				phoneNumber: "088 4950964"
			};
	
			createErrorResponse(
				dormProps,
				400,
				"dorm validation failed: name: Dorm name is required!",
				done
			);
		});
	
		it("without authorization fails", done => {
			const dormProps = {
				name: "TestDorm2",
				streetName: "TestStreet",
				houseNumber: "2",
				postalCode: "1000AA",
				cityName: "City1",
				phoneNumber: "088 4950964"
			};
	
			requester.post(baseRoute)
				.send(dormProps)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("without streetName fails", done => {
			const dormProps = {
				name: "TestDorm2",
				houseNumber: "2",
				postalCode: "1000AA",
				cityName: "City1",
				phoneNumber: "088 4950964"
			};

			createErrorResponse(
				dormProps,
				400,
				"dorm validation failed: streetName: Street name is required!",
				done
			);
		});

		it("without houseNumber fails", done => {
			const dormProps = {
				name: "TestDorm2",
				streetName: "TestStreet",
				postalCode: "1000AA",
				cityName: "City1",
				phoneNumber: "088 4950964"
			};

			createErrorResponse(
				dormProps,
				400,
				"dorm validation failed: houseNumber: House number is required!",
				done
			);
		});

		it("without cityName fails", done => {
			const dormProps = {
				name: "TestDorm2",
				streetName: "TestStreet",
				houseNumber: "2",
				postalCode: "1000AA",
				phoneNumber: "088 4950964"
			};

			createErrorResponse(
				dormProps,
				400,
				"dorm validation failed: cityName: City name is required!",
				done
			);
		});

		it("without phoneNumber fails", done => {
			const dormProps = {
				name: "TestDorm2",
				streetName: "TestStreet",
				houseNumber: "2",
				postalCode: "1000AA",
				cityName: "City1"
			};

			createErrorResponse(
				dormProps,
				400,
				"dorm validation failed: phoneNumber: Phone number is required!",
				done
			);
		});
	});

	describe("a PUT to /api/studenthome/:StudentHomeId", () => {
		it("updates a dorm", done => {
			const dormProps = {
				name: "NewDormName",
				houseNumber: "42",
				phoneNumber: "076 521 4500"
			};

			requester.put(`${baseRoute}/${dormId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(dormProps)
				.end((_, res) => {
					Dorm.findOne({ 
						streetName: "Street1",
						houseNumber: "42",
						postalCode: "1000AA",
						cityName: "City1" 
					}).then(dorm => {
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("_id", dorm._id.toString());
						expect(res.body).to.haveOwnProperty("name", "NewDormName");
						expect(res.body).to.haveOwnProperty("maker", dorm.maker.toString());
						expect(res.body).to.haveOwnProperty("streetName", "Street1");
						expect(res.body).to.haveOwnProperty("houseNumber", "42");
						expect(res.body).to.haveOwnProperty("postalCode", "1000AA");
						expect(res.body).to.haveOwnProperty("cityName", "City1");
						expect(res.body).to.haveOwnProperty("phoneNumber", "076 521 4500");
						done();
					});
				});
		});

		it("with empty body doesn't update", done => {
			const dormProps = {
			};

			requester.put(`${baseRoute}/${dormId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(dormProps)
				.end((_, res) => {
					Dorm.findOne({ 
						streetName: "Street1",
						houseNumber: "1",
						postalCode: "1000AA",
						cityName: "City1" 
					}).then(dorm => {
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("_id", dorm._id.toString());
						expect(res.body).to.haveOwnProperty("name", "TestDorm1");
						expect(res.body).to.haveOwnProperty("maker", dorm.maker.toString());
						expect(res.body).to.haveOwnProperty("streetName", "Street1");
						expect(res.body).to.haveOwnProperty("houseNumber", "1");
						expect(res.body).to.haveOwnProperty("postalCode", "1000AA");
						expect(res.body).to.haveOwnProperty("cityName", "City1");
						expect(res.body).to.haveOwnProperty("phoneNumber", "088 4950964");
						done();
					});
				});
		});

		it("without authorization fails", done => {
			const dormProps = {
				name: "NewDormName",
				houseNumber: "42",
				phoneNumber: "076 521 4500"
			};
	
			requester.put(`${baseRoute}/${dormId}`)
				.send(dormProps)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if authorization secret is missing on server", done => {
			const tempToken =  config.security.jwt.accessToken;
			config.security.jwt.accessToken = undefined;
			const dormProps = {
				name: "NewDormName",
				houseNumber: "42",
				phoneNumber: "076 521 4500"
			};
	
			requester.put(`${baseRoute}/${dormId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(dormProps)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					config.security.jwt.accessToken = tempToken;
					done();
				});
		});
	
		it("fails if id doesn't exist", done => {
			const dormProps = {
				name: "NewDormName",
				houseNumber: "42",
				phoneNumber: "076 521 4500"
			};
	
			requester.put(`${baseRoute}/${new ObjectId()}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(dormProps)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", 
						"No dorm with this ID or you are not authorized to update this dorm");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if user is not the maker of the dorm", done => {
			const dormProps = {
				name: "NewDormName",
				houseNumber: "42",
				phoneNumber: "076 521 4500"
			};
	
			User.create({
				firstName: "AnotherFirstTestName",
				lastName: "AnotherLastTestName",
				studentNumber: "1",
				emailAddress: "another.email@test.nl",
				password: "AnotherTestPassowrd"
			}).then(user => {
				const tempAuthToken = UserService.signToken(user).token;
				requester.put(`${baseRoute}/${dormId}`)
					.set("Authorization", `Bearer ${tempAuthToken}`)
					.send(dormProps)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error", 
							"No dorm with this ID or you are not authorized to update this dorm");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						done();
					});
			});
		});

		it("fails if phoneNumber is invalid", done => {
			const dormProps = {
				name: "NewDormName",
				houseNumber: "42",
				phoneNumber: "InvalidPhoneNumber"
			};
	
			requester.put(`${baseRoute}/${dormId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(dormProps)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", 
						`Validation failed: phoneNumber: Path \`phoneNumber\` is invalid (${dormProps.phoneNumber}).`);
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if postalCode is invalid", done => {
			const dormProps = {
				name: "NewDormName",
				houseNumber: "42",
				postalCode: "InvalidPostalCode"
			};
	
			requester.put(`${baseRoute}/${dormId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(dormProps)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", 
						`Validation failed: postalCode: Path \`postalCode\` is invalid (${dormProps.postalCode}).`);
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	});

	describe("a DELETE to /api/studenthome/:StudentHomeId", () => {
		it("deletes a dorm", done => {
			Dorm.countDocuments().then(oldCount => {
				requester.delete(`${baseRoute}/${dormId}`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((_, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("_id", dormId.toString());
						expect(res.body).to.haveOwnProperty("name", "TestDorm1");
						expect(res.body).to.haveOwnProperty("maker", storedUser._id.toString());
						expect(res.body).to.haveOwnProperty("streetName", "Street1");
						expect(res.body).to.haveOwnProperty("houseNumber", "1");
						expect(res.body).to.haveOwnProperty("postalCode", "1000AA");
						expect(res.body).to.haveOwnProperty("cityName", "City1");
						expect(res.body).to.haveOwnProperty("phoneNumber", "088 4950964");
						Dorm.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount - 1);
							done();
						});
					});
			});
		});

		it("fails if id doesn't exist", done => {
	
			Dorm.countDocuments().then(oldCount => {
				requester.delete(`${baseRoute}/${new ObjectId()}`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error", 
							"No dorm with this ID or you are not authorized to delete this dorm");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						Dorm.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
			});
		});

		it("fails if user is not authorized", done => {
	
			Dorm.countDocuments().then(oldCount => {
				requester.delete(`${baseRoute}/${dormId}`)
					.end((_, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						Dorm.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
			});
		});

		it("fails if user is not the maker of the dorm", done => {
	
			User.create({
				firstName: "AnotherFirstTestName",
				lastName: "AnotherLastTestName",
				studentNumber: "1",
				emailAddress: "another.email@test.nl",
				password: "AnotherTestPassowrd"
			}).then(user => {
				const tempAuthToken = UserService.signToken(user).token;
				Dorm.countDocuments().then(oldCount => {
					requester.delete(`${baseRoute}/${dormId}`)
						.set("Authorization", `Bearer ${tempAuthToken}`)
						.end((_, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", 
								"No dorm with this ID or you are not authorized to delete this dorm");
							expect(res.body).to.haveOwnProperty("date", config.errorDate());
							Dorm.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
				});
			});
		});
	});

	describe("GET to /api/studenthome", () => {
		let getDormId;
		let getUserId;

		const beforeTests = () => 
			Dorm.collection.drop()
				.then(() => User.collection.drop())
				.then(() => User.create({
					firstName: "OtherFirstTestName",
					lastName: "OtherLastTestName",
					studentNumber: "1",
					emailAddress: "test2@test.nl",
					password: "TestPassowrd"
				}))
				.then(user => {
					getUserId = user._id;
					return Dorm.create({
						name: "TestDorm1",
						maker: storedUser._id,
						streetName: "Street1",
						houseNumber: "1",
						postalCode: "1000AA",
						cityName: "City1",
						phoneNumber: "088 4950964"
					});
				})
				.then(dorm => {
					getDormId = dorm._id;
					return Dorm.create({
						name: "TestDorm2",
						maker: getUserId,
						streetName: "Street1",
						houseNumber: "2",
						postalCode: "1000AA",
						cityName: "City1",
						phoneNumber: "088 4950964"
					});
				})
				.then(() => {
					return Dorm.create({
						name: "TestDorm3",
						maker: getUserId,
						streetName: "Street1",
						houseNumber: "3",
						postalCode: "1000AA",
						cityName: "City1",
						phoneNumber: "088 4950964"
					});
				})
				.then(() => {
					return Dorm.create({
						name: "OtherDorm1",
						maker: getUserId,
						streetName: "Street1",
						houseNumber: "1",
						postalCode: "1000AA",
						cityName: "OtherCity",
						phoneNumber: "088 4950964"
					});
				})
				.then(() => {
					return Dorm.create({
						name: "OtherDorm2",
						maker: getUserId,
						streetName: "Street1",
						houseNumber: "2",
						postalCode: "1000AA",
						cityName: "OtherCity",
						phoneNumber: "088 4950964"
					});
				})
				.then(() => {
					return Dorm.create({
						name: "OtherDormOther",
						maker: getUserId,
						streetName: "Street1",
						houseNumber: "3",
						postalCode: "1000AA",
						cityName: "AnotherOtherCity",
						phoneNumber: "088 4950964"
					});
				});

		it("gets all dorms", done => {
			beforeTests()
				.then(() => {
					requester.get(`${baseRoute}`)
						.end((_, res) => {
							expect(res).to.have.status(200);
							expect(res.body).to.have.property("results").and.to.have.lengthOf(6);

							const dorm = res.body.results.find(dorm => dorm._id == getDormId);

							expect(dorm).to.haveOwnProperty("name", "TestDorm1");
							expect(dorm).to.haveOwnProperty("cityName", "City1");
							done();
						});
				});
		});
	
		it("returns no dorms if there are none", done => {
			Dorm.collection.drop()
				.then(() => {
					requester.get(`${baseRoute}`)
						.end((error, res) => {
							expect(res).to.have.status(200);
							expect(res.body).to.haveOwnProperty("results").and.to.be.empty;
							done();
						});
				});
		});

		it("gets all dorms that are in City1", done => {
			beforeTests()
				.then(() => {
					requester.get(`${baseRoute}?city=City1`)
						.end((_, res) => {
							expect(res).to.have.status(200);
							expect(res.body).to.have.property("results").and.to.have.lengthOf(3);
							res.body.results.forEach(dorm => {
								expect(dorm).to.haveOwnProperty("name").match(/^TestDorm[123]/);
								expect(dorm).to.haveOwnProperty("cityName", "City1");
							});
							done();
						});
				});
		});

		it("gets all dorms that have a name starting with OtherDorm", done => {
			beforeTests()
				.then(() => {
					requester.get(`${baseRoute}?name=OtherDorm`)
						.end((_, res) => {
							expect(res).to.have.status(200);
							expect(res.body).to.have.property("results").and.to.have.lengthOf(3);
							res.body.results.forEach(dorm => {
								expect(dorm).to.haveOwnProperty("name").match(/^OtherDorm[1|2|Other]/);
								expect(dorm).to.haveOwnProperty("cityName").match(/[Another]?OtherCity$/);
							});
							done();
						});
				});
		});

		it("gets all dorms that have a name starting with OtherDormOther and in city OtherCity", done => {
			beforeTests()
				.then(() => {
					requester.get(`${baseRoute}?name=OtherDormOther&city=AnotherOtherCity`)
						.end((_, res) => {
							expect(res).to.have.status(200);
							expect(res.body).to.have.property("results").and.to.have.lengthOf(1);
							expect(res.body.results[0]).to.haveOwnProperty("name", "OtherDormOther");
							expect(res.body.results[0]).to.haveOwnProperty("cityName", "AnotherOtherCity");
							done();
						});
				});
		});
	});

	describe("GET to /api/studenthome/:StudentHomeId", () => {
		it("gets the details of a dorm", done => {
			requester.get(`${baseRoute}/${dormId}`)
				.end((_, res) => {
					expect(res.body).to.haveOwnProperty("_id", dormId.toString());
					expect(res.body).to.haveOwnProperty("name", "TestDorm1");
					expect(res.body).to.haveOwnProperty("maker");
					expect(res.body.maker).to.haveOwnProperty("firstName", storedUser.firstName);
					expect(res.body.maker).to.haveOwnProperty("lastName", storedUser.lastName);
					expect(res.body.maker).to.haveOwnProperty("studentNumber", storedUser.studentNumber);
					expect(res.body.maker).to.haveOwnProperty("emailAddress", storedUser.emailAddress);
					expect(res.body).to.haveOwnProperty("streetName", "Street1");
					expect(res.body).to.haveOwnProperty("houseNumber", "1");
					expect(res.body).to.haveOwnProperty("postalCode", "1000AA");
					expect(res.body).to.haveOwnProperty("cityName", "City1");
					expect(res.body).to.haveOwnProperty("phoneNumber", "088 4950964");
					done();
				});
		});

		it("GET to /api/studenthome/:id fails if id doesn't exist", done => {
			requester.get(`${baseRoute}/${new ObjectId()}`)
				.end((error, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "No dorm with this ID or you are not authorized to get this dorm");
					expect(res.body).to.haveOwnProperty("date",config.errorDate());
					done();
				});
		});
	});

	describe("PUT to /api/studenthome/:StudentHomeId/user", () => {
		it("adds the currently logged in user to the dorm", done => {
			User.create({
				firstName: "Temp firstname",
				lastName: "Temp lastname",
				emailAddress: "unused.email@test.com",
				studentNumber: "1234",
				password: "Bad Password"
			}).then(user => {
				requester.put(`${baseRoute}/${dormId}/user`)
					.set("Authorization", `Bearer ${authToken}`)
					.send({ userId: user._id })
					.end((_, res) => {
						Dorm.findOne({
							streetName: "Street1",
							houseNumber: "1",
							postalCode: "1000AA",
							cityName: "City1"
						}).then(dorm => {
							expect(res).to.have.status(200);
							expect(res.body).to.haveOwnProperty("_id", dorm._id.toString());
							expect(res.body).to.haveOwnProperty("students").and.to.have.lengthOf(1);
							expect(res.body.students[0]).to.equal(user._id.toString());
							done();
						});
					});
			});
		});

		it("fails if user is not authorized", done => {
			requester.put(`${baseRoute}/${dormId}/user`)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if user doesn't exist", done => {
			requester.put(`${baseRoute}/${dormId}/user`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({ userId: new ObjectId() })
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error",
						"No user on given id!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if dorm doesn't exist", done => {
			requester.put(`${baseRoute}/${new ObjectId()}/user`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error",
						"No Dorm found!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if user was already added to dorm", done => {
			Dorm.findById(dormId).then(dorm => {
				dorm.students.push(storedUser._id);
				return dorm.save();
			}).then(() => {
				requester.put(`${baseRoute}/${dormId}/user`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error",
							"No duplicate students allowed!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						done();
					});
			});
		});
	});
});
