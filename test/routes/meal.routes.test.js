/* eslint-disable no-undef */
const chai = require("chai");
const expect = chai.expect;
const config = require("../../src/config/config");
const logger = config.logger;

const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const requester = require("../../requester");

//Models:
const Dorm = require("../../src/models/dorm.model");
const User = require("../../src/models/user.model");

//User service for token
const UserService = require("../../src/services/user.service");


describe("In testing the meal router, ", function() {
	this.retries(1);
	
	let baseRoute = "/api/studenthome/:dormId/meal";
	let dormId;
	let mealId;
	let storedUser;
	let authToken;

	const createErrorResponse = (properties, statusCode, errorMessage, done) => {
		Dorm.countDocuments().then(oldCount => {
			requester.post(baseRoute)
				.set("Authorization", `Bearer ${authToken}`)
				.send(properties)
				.end((_, res) => {
					expect(res).to.have.status(statusCode);
					expect(res.body).to.haveOwnProperty("error").and.to.match(errorMessage);
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					Dorm.countDocuments().then(newCount => {
						expect(newCount).to.equal(oldCount);
						done();
					});
				});
		});
	};

	beforeEach(done => {
		User.create({
			firstName: "FirstTestName",
			lastName: "LastTestName",
			studentNumber: "0",
			emailAddress: "test@test.nl",
			password: "TestPassowrd"
		}).then(user => {
			storedUser = user;
			authToken = UserService.signToken(user).token;

			return Dorm.create({
				name: "TestDorm1",
				maker: storedUser,
				streetName: "Street1",
				houseNumber: "1",
				postalCode: "1000AA",
				cityName: "City1",
				phoneNumber: "088 4950964"
			});
		}).then(newDorm => {
			baseRoute = `/api/studenthome/${newDorm._id}/meal`;
			dormId = newDorm._id;
			newDorm.meals.push({
				name: "Test Meal",
				maker: storedUser._id,
				postedOn: new Date(2020, 5, 3),
				offeredOn: new Date(2020, 5, 3),
				price: "0",
				allergyInfo: "Allergy Info",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 2
			});
			return newDorm.save({ validateBeforeSave: true });
		}).then(dorm => {
			mealId = dorm.meals[0]._id;
			done();
		})
			.catch(error => {
				logger.error("Something went wrong!");
				logger.error(error);
			});
	});

	describe("a POST to /api/studenthome/:dormId/meal", () => {
		it("creates a new meal", done => {
			const date = Date.now(); //today
			const mealProps = {
				name: "Test Meal2",
				offeredOn: date,
				postedOn: date,
				price: "0",
				allergyInfo: "None",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 1
			};
	
			Dorm.findById(dormId).then(oldDorm => {
				const oldCount = oldDorm.meals.length;
				requester.post(baseRoute)
					.set("Authorization", `Bearer ${authToken}`)
					.send(mealProps)
					.end((_, res) => {
						Dorm.findById(dormId)
							.then(dorm => {
								expect(res).to.have.status(201);
								expect(res.body).to.haveOwnProperty("_id", 
									dorm.meals[dorm.meals.length - 1]._id.toString());
								expect(res.body).to.haveOwnProperty("name", "Test Meal2");
								expect(res.body).to.haveOwnProperty("maker", storedUser._id.toString());
								expect(res.body).to.haveOwnProperty("postedOn", date);
								expect(res.body).to.haveOwnProperty("offeredOn", date);
								expect(res.body).to.haveOwnProperty("participants").to.be.instanceOf(Array).and.to.be.empty;
								expect(res.body).to.haveOwnProperty("ingredients").to.be.instanceOf(Array).and.to.eql(["Ingredient1"]);
								expect(res.body).to.haveOwnProperty("price", "0");
								expect(res.body).to.haveOwnProperty("maxParticipants", 1);
								expect(res.body).to.haveOwnProperty("allergyInfo", "None");
								expect(dorm.meals.length).to.equal(oldCount + 1);
								done();
							});
					});
			});
		});

		it("without name fails", done => {
			const mealProps = {
				offeredOn: Date.now(), //today
				price: "0",
				allergyInfo: "None",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 1
			};
	
			createErrorResponse(
				mealProps,
				400,
				/^dorm validation failed: meals.\d.name: Meal name is required!$/,
				done
			);
		});
	
		it("fails when user is not part of the studenthouse", done => {
			const mealProps = {
				name: "Test Meal 2",
				offeredOn: Date.now(), //today
				price: "0",
				allergyInfo: "None",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 1
			};

			let oldCount;
	
			Dorm.findById(dormId).then(dorm => {
				oldCount = dorm.meals.length;
				return User.create({
					firstName: "AnotherFirstTestName",
					lastName: "AnotherLastTestName",
					studentNumber: "1",
					emailAddress: "another.email@test.nl",
					password: "AnotherTestPassowrd"
				});
			}).then(user => {
				const tempAuthToken = UserService.signToken(user).token;
				requester.post(baseRoute)
					.set("Authorization", `Bearer ${tempAuthToken}`)
					.send(mealProps)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error",
							"You must be a student of the dorm in order to make a meal for it!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						Dorm.findById(dormId).then(dorm => {
							expect(dorm.meals.length).to.equal(oldCount);
							done();
						});
					});
			});
		});
	
		it("without authorization fails", done => {
			const mealProps = {
				name: "Test Meal2",
				offeredOn: Date.now(), //today
				price: "0",
				allergyInfo: "None",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 1
			};
	
			Dorm.findById(dormId).then(dorm => {
				const oldCount = dorm.meals.length;
				requester.post(baseRoute)
					.send(mealProps)
					.end((_, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						Dorm.findById(dormId).then(dorm => {
							expect(dorm.meals.length).to.equal(oldCount);
							done();
						});
					});
			});
		});

		it("without offered date fails", done => {
			const mealProps = {
				name: "Test Meal2",
				price: "0",
				allergyInfo: "None",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 1
			};
	
			createErrorResponse(
				mealProps,
				400,
				/^dorm validation failed: meals.\d.offeredOn: Cast to Date failed for value "Invalid Date" at path "offeredOn"$/,
				done
			);
		});

		it("without price fails", done => {
			const mealProps = {
				name: "Test Meal2",
				offeredOn: Date.now(), //today
				allergyInfo: "None",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 1
			};
	
			createErrorResponse(
				mealProps,
				400,
				/^dorm validation failed: meals.\d.price: Meal price is required!$/,
				done
			);
		});

		it("with negative price fails", done => {
			const mealProps = {
				name: "Test Meal2",
				offeredOn: Date.now(), //today
				price: "-1",
				allergyInfo: "None",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 1
			};
	
			createErrorResponse(
				mealProps,
				400,
				/^dorm validation failed: meals.\d.price: Meal price must be positive!$/,
				done
			);
		});

		it("without allergyInfo fails", done => {
			const mealProps = {
				name: "Test Meal2",
				offeredOn: Date.now(), //today
				price: "0",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 1
			};
	
			createErrorResponse(
				mealProps,
				400,
				/^dorm validation failed: meals.\d.allergyInfo: Meal allergy information is required!$/,
				done
			);
		});

		it("without ingredients fails", done => {
			const mealProps = {
				name: "Test Meal2",
				offeredOn: Date.now(), //today
				price: "0",
				allergyInfo: "None",
				maxParticipants: 1
			};
	
			createErrorResponse(
				mealProps,
				400,
				/^dorm validation failed: meals.\d.ingredients: Meal ingredients must be specified and may not be empty!$/,
				done
			);
		});

		it("with empty ingredients fails", done => {
			const mealProps = {
				name: "Test Meal2",
				offeredOn: Date.now(), //today
				price: "0",
				allergyInfo: "None",
				ingredients: [],
				maxParticipants: 1
			};
	
			createErrorResponse(
				mealProps,
				400,
				/^dorm validation failed: meals.\d.ingredients: Meal ingredients must be specified and may not be empty!$/,
				done
			);
		});

		it("without maxParticipants fails", done => {
			const mealProps = {
				name: "Test Meal2",
				offeredOn: Date.now(), //today
				price: "0",
				allergyInfo: "None",
				ingredients: [ "Ingredient1" ]
			};
	
			createErrorResponse(
				mealProps,
				400,
				/^dorm validation failed: meals.\d.maxParticipants: Meal maximum number of participants is required!$/,
				done
			);
		});

		it("with negative maxParticipants fails", done => {
			const mealProps = {
				name: "Test Meal2",
				offeredOn: Date.now(), //today
				price: "0",
				allergyInfo: "None",
				ingredients: [ "Ingredient1" ],
				maxParticipants: -1
			};
	
			createErrorResponse(
				mealProps,
				400,
				/^dorm validation failed: meals.\d.maxParticipants: Meal maximum number of participants must be greater than zero!$/,
				done
			);
		});
	});

	describe("a PUT to /api/studenthome/:dormId/meal/:mealId", () => {
		it("updates a meal", done => {
			const date = Date.now();
			const mealProps = {
				name: "New Test Meal",
				offeredOn: date,
				price: "1"
			};

			requester.put(`${baseRoute}/${mealId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(mealProps)
				.end((_, res) => {
					Dorm.findById(dormId).then(dorm => {
						const meal = dorm.meals.find(meal => meal._id.toString() == mealId.toString());
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("_id", meal._id.toString());
						expect(res.body).to.haveOwnProperty("name", "New Test Meal");
						expect(res.body).to.haveOwnProperty("maker", storedUser._id.toString());
						expect(res.body).to.haveOwnProperty("postedOn", new Date(2020, 5, 3).getTime());
						expect(res.body).to.haveOwnProperty("offeredOn", date);
						expect(res.body).to.haveOwnProperty("participants").to.be.instanceOf(Array).and.to.be.empty;
						expect(res.body).to.haveOwnProperty("ingredients").to.be.instanceOf(Array).and.to.eql(["Ingredient1"]);
						expect(res.body).to.haveOwnProperty("price", "1");
						expect(res.body).to.haveOwnProperty("maxParticipants", 2);
						expect(res.body).to.haveOwnProperty("allergyInfo", "Allergy Info");
						done();
					});
				});
		});

		it("with empty body doesn't update", done => {
			const mealProps = {
			};

			requester.put(`${baseRoute}/${mealId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(mealProps)
				.end((_, res) => {
					Dorm.findById(dormId).then(dorm => {
						const meal = dorm.meals.find(meal => meal._id.toString() == mealId.toString());
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("_id", meal._id.toString());
						expect(res.body).to.haveOwnProperty("name", "Test Meal");
						expect(res.body).to.haveOwnProperty("maker", storedUser._id.toString());
						expect(res.body).to.haveOwnProperty("postedOn", new Date(2020, 5, 3).getTime());
						expect(res.body).to.haveOwnProperty("offeredOn", new Date(2020, 5, 3).getTime());
						expect(res.body).to.haveOwnProperty("participants").to.be.instanceOf(Array).and.to.be.empty;
						expect(res.body).to.haveOwnProperty("ingredients").to.be.instanceOf(Array).and.to.eql(["Ingredient1"]);
						expect(res.body).to.haveOwnProperty("price", "0");
						expect(res.body).to.haveOwnProperty("maxParticipants", 2);
						expect(res.body).to.haveOwnProperty("allergyInfo", "Allergy Info");
						done();
					});
				});
		});

		it("without authorization fails", done => {
			const date = Date.now();
			const mealProps = {
				name: "New Test Meal",
				offeredOn: date,
				price: "1"
			};
	
			requester.put(`${baseRoute}/${mealId}`)
				.send(mealProps)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	
		it("fails if id doesn't exist", done => {
			const date = Date.now();
			const mealProps = {
				name: "New Test Meal",
				offeredOn: date,
				price: "1"
			};
	
			requester.put(`${baseRoute}/${new ObjectId()}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(mealProps)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", 
						"No meal with this ID or you are not authorized to update this meal");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if dorm doesn't exist", done => {
			const date = Date.now();
			const mealProps = {
				name: "New Test Meal",
				offeredOn: date,
				price: "1"
			};
	
			requester.put(`/api/studenthome/${new ObjectId()}/meal/${mealId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(mealProps)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", 
						"Dorm doesn't exist!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if user is not the maker of the meal", done => {
			const date = Date.now();
			const mealProps = {
				name: "New Test Meal",
				offeredOn: date,
				price: "1"
			};
	
			User.create({
				firstName: "AnotherFirstTestName",
				lastName: "AnotherLastTestName",
				studentNumber: "1",
				emailAddress: "another.email@test.nl",
				password: "AnotherTestPassowrd"
			}).then(user => {
				const tempAuthToken = UserService.signToken(user).token;
				requester.put(`${baseRoute}/${mealId}`)
					.set("Authorization", `Bearer ${tempAuthToken}`)
					.send(mealProps)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error", 
							"No meal with this ID or you are not authorized to update this meal");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						done();
					});
			});
		});

		it("fails if price is negative", done => {
			const date = Date.now();
			const mealProps = {
				name: "New Test Meal",
				offeredOn: date,
				price: "-1"
			};
	
			requester.put(`${baseRoute}/${mealId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(mealProps)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error")
						.and.to.match(/^Validation failed: meals.0.price: Meal price must be positive!$/);
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if ingredients is empty", done => {
			const date = Date.now();
			const mealProps = {
				name: "New Test Meal",
				offeredOn: date,
				price: "1",
				ingredients: []
			};
	
			requester.put(`${baseRoute}/${mealId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(mealProps)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error")
						.and.to.match(/^Validation failed: meals.\d.ingredients: Meal ingredients must be specified and may not be empty!$/);
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	});

	describe("a DELETE to /api/studenthome/:dormId/meal/:mealId", () => {
		it("deletes a meal", done => {
			Dorm.findById(dormId).then(dorm => {
				const oldCount = dorm.meals.length;
				requester.delete(`${baseRoute}/${mealId}`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((_, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("_id", mealId.toString());
						expect(res.body).to.haveOwnProperty("name", "Test Meal");
						expect(res.body).to.haveOwnProperty("maker", storedUser._id.toString());
						expect(res.body).to.haveOwnProperty("postedOn", new Date(2020, 5, 3).getTime());
						expect(res.body).to.haveOwnProperty("offeredOn", new Date(2020, 5, 3).getTime());
						expect(res.body).to.haveOwnProperty("participants").to.be.instanceOf(Array).and.to.be.empty;
						expect(res.body).to.haveOwnProperty("ingredients").to.be.instanceOf(Array).and.to.eql(["Ingredient1"]);
						expect(res.body).to.haveOwnProperty("price", "0");
						expect(res.body).to.haveOwnProperty("maxParticipants", 2);
						expect(res.body).to.haveOwnProperty("allergyInfo", "Allergy Info");
						Dorm.findById(dormId).then(dorm => {
							expect(dorm.meals.length).to.equal(oldCount - 1);
							done();
						});
					});
			});
		});

		it("fails if meal doesn't exist", done => {
	
			Dorm.findById(dormId).then(dorm => {
				const oldCount = dorm.meals.length;
				requester.delete(`${baseRoute}/${new ObjectId()}`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error", 
							"No meal with this ID or you are not authorized to delete this meal");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						Dorm.findById(dormId).then(dorm => {
							expect(dorm.meals.length).to.equal(oldCount);
							done();
						});
					});
			});
		});

		it("fails if dorm doesn't exist", done => {
			const date = Date.now();
			const mealProps = {
				name: "New Test Meal",
				offeredOn: date,
				price: "1"
			};

			Dorm.findById(dormId).then(dorm => {
				const oldCount = dorm.meals.length;
				requester.delete(`/api/studenthome/${new ObjectId()}/meal/${mealId}`)
					.set("Authorization", `Bearer ${authToken}`)
					.send(mealProps)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error", 
							"Dorm doesn't exist!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						Dorm.findById(dormId).then(dorm => {
							expect(dorm.meals.length).to.equal(oldCount);
							done();
						});
					});
			});
		});

		it("fails if user is not authorized", done => {
	
			Dorm.findById(dormId).then(dorm => {
				const oldCount = dorm.meals.length;
				requester.delete(`${baseRoute}/${dormId}`)
					.end((_, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						Dorm.findById(dormId).then(dorm => {
							expect(dorm.meals.length).to.equal(oldCount);
							done();
						});
					});
			});
		});

		it("fails if user is not the maker of the meal", done => {
	
			User.create({
				firstName: "AnotherFirstTestName",
				lastName: "AnotherLastTestName",
				studentNumber: "1",
				emailAddress: "another.email@test.nl",
				password: "AnotherTestPassowrd"
			}).then(user => {
				const tempAuthToken = UserService.signToken(user).token;
				Dorm.findById(dormId).then(dorm => {
					const oldCount = dorm.meals.length;
					requester.delete(`${baseRoute}/${dormId}`)
						.set("Authorization", `Bearer ${tempAuthToken}`)
						.end((_, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", 
								"No meal with this ID or you are not authorized to delete this meal");
							expect(res.body).to.haveOwnProperty("date", config.errorDate());
							Dorm.findById(dormId).then(dorm => {
								expect(dorm.meals.length).to.equal(oldCount);
								done();
							});
						});
				});
			});
		});
	});

	describe("GET to /api/studenthome/:dormId/meal", () => {
		let getDormId;
		let getUserId;
		let getMealId;

		const beforeTests = () => 
			Dorm.collection.drop()
				.then(() => User.collection.drop())
				.then(() => User.create({
					firstName: "FirstTestName",
					lastName: "LastTestName",
					studentNumber: "1",
					emailAddress: "test2@test.nl",
					password: "TestPassowrd"
				}))
				.then(user => {
					getUserId = user._id;
					return Dorm.create({
						name: "TestDorm1",
						maker: getUserId,
						streetName: "Street1",
						houseNumber: "1",
						postalCode: "1000AA",
						cityName: "City1",
						phoneNumber: "088 4950964"
					}).then(dorm => {
						dorm.meals.push({
							name: "Test Meal 1",
							maker: getUserId,
							postedOn: new Date(2020, 5, 3),
							offeredOn: new Date(2020, 5, 3),
							price: "0",
							allergyInfo: "Allergy Info",
							ingredients: [ "Ingredient 1" ],
							maxParticipants: 1
						});

						dorm.meals.push({
							name: "Test Meal 2",
							maker: getUserId,
							postedOn: new Date(2019, 5, 3),
							offeredOn: new Date(2019, 5, 3),
							price: "0",
							allergyInfo: "Allergy Info",
							ingredients: [
								"Ingredient 1",
								"Ingredient 2"
							],
							maxParticipants: 1
						});

						return dorm.save();
					});
				})
				.then(dorm => {
					getDormId = dorm._id;
					getMealId = dorm.meals[0]._id;
					return Dorm.create({
						name: "TestDorm2",
						maker: getUserId,
						streetName: "Street1",
						houseNumber: "2",
						postalCode: "1000AA",
						cityName: "City1",
						phoneNumber: "088 4950964"
					}).then(dorm => {
						dorm.meals.push({
							name: "Test Meal 3",
							maker: getUserId,
							postedOn: new Date(2018, 5, 3),
							offeredOn: new Date(2021, 5, 3),
							price: "0",
							allergyInfo: "Allergy Info",
							ingredients: [ "Ingredient 1" ],
							maxParticipants: 1
						});

						dorm.meals.push({
							name: "Test Meal 4",
							maker: getUserId,
							postedOn: new Date(2017, 5, 3),
							offeredOn: new Date(2019, 5, 3),
							price: "0",
							allergyInfo: "Allergy Info",
							ingredients: [
								"Ingredient 1",
								"Ingredient 2",
								"Ingredient 3"
							],
							maxParticipants: 1
						});

						return dorm.save();
					});
				});

		it("gets all meals of a dorm", done => {
			beforeTests()
				.then(() => {
					requester.get(`/api/studenthome/${getDormId}/meal`)
						.end((_, res) => {
							User.findById(getUserId).then(user => {
								expect(res).to.have.status(200);
								expect(res.body).to.have.property("results").and.to.have.lengthOf(2);

								const meal = res.body.results.find(meal => meal._id == getMealId);

								expect(meal).to.haveOwnProperty("name", "Test Meal 1");
								expect(meal).to.haveOwnProperty("price", "0");
								expect(meal).to.haveOwnProperty("maker");
								expect(meal.maker).to.haveOwnProperty("firstName", user.firstName);
								expect(meal.maker).to.haveOwnProperty("lastName", user.lastName);
								expect(meal.maker).to.haveOwnProperty("studentNumber", user.studentNumber);
								expect(meal.maker).to.haveOwnProperty("emailAddress", user.emailAddress);
								expect(meal).to.haveOwnProperty("offeredOn", new Date(2020, 5, 3).getTime());
								done();
							});
						});
				});
		});

		it("returns no meals if there are none", done => {
			beforeTests()
				.then(() => Dorm.findById(getDormId))
				.then(dorm => {
					dorm.meals = [];
					return dorm.save();
				}).then(() => {
					requester.get(`/api/studenthome/${getDormId}/meal`)
						.end((_, res) => {
							expect(res).to.have.status(200);
							expect(res.body).to.haveOwnProperty("results").and.to.be.empty;
							done();
						});
				});
		});
	});

	describe("GET to /api/studenthome/:dormId/meal/:mealId", () => {
		it("gets the details of a dorm", done => {
			requester.get(`${baseRoute}/${mealId}`)
				.end((_, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("_id", mealId.toString());
					expect(res.body).to.haveOwnProperty("name", "Test Meal");
					expect(res.body).to.haveOwnProperty("maker");
					expect(res.body.maker).to.haveOwnProperty("firstName", storedUser.firstName);
					expect(res.body.maker).to.haveOwnProperty("lastName", storedUser.lastName);
					expect(res.body.maker).to.haveOwnProperty("studentNumber", storedUser.studentNumber);
					expect(res.body.maker).to.haveOwnProperty("emailAddress", storedUser.emailAddress);
					expect(res.body).to.haveOwnProperty("postedOn", new Date(2020, 5, 3).getTime());
					expect(res.body).to.haveOwnProperty("offeredOn", new Date(2020, 5, 3).getTime());
					expect(res.body).to.haveOwnProperty("participants").to.be.instanceOf(Array).and.to.be.empty;
					expect(res.body).to.haveOwnProperty("ingredients").to.be.instanceOf(Array).and.to.eql(["Ingredient1"]);
					expect(res.body).to.haveOwnProperty("price", "0");
					expect(res.body).to.haveOwnProperty("maxParticipants", 2);
					expect(res.body).to.haveOwnProperty("allergyInfo", "Allergy Info");
					done();
				});
		});

		it("GET to /api/studenthome/:id fails if id doesn't exist", done => {
			requester.get(`${baseRoute}/${new ObjectId()}`)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "No meal with this ID or you are not authorized to get this meal");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	});

	describe("POST to /api/studenthome/:dormId/meal/:mealId/signup", () => {
		it("adds the currently logged in user as participant to the meal", done => {
			User.create({
				firstName: "AnotherFirstTestName",
				lastName: "AnotherLastTestName",
				studentNumber: "1",
				emailAddress: "another.email@test.nl",
				password: "AnotherTestPassowrd"
			}).then(user => {
				const tempUserId = user._id;
				const tempAuthToken = UserService.signToken(user).token;
				requester.post(`${baseRoute}/${mealId}/signup`)
					.set("Authorization", `Bearer ${tempAuthToken}`)
					.end((_, res) => {
						Dorm.findById(dormId).then(dorm => 
							dorm.meals.find(meal => meal._id.toString() == mealId.toString())
						).then(meal => {
							expect(res).to.have.status(201);
							expect(meal._doc).to.haveOwnProperty("participants").and.to.be.instanceOf(Array).and.to.have.lengthOf(1);
							expect(meal.participants[0]._doc).to.deep.haveOwnProperty("user", tempUserId);
							expect(meal.participants[0]._doc).to.haveOwnProperty("postedOn");
							const postedDay = new Date(meal.participants[0].postedOn).setMinutes(0, 0, 0);
							expect(postedDay).to.equal(new Date().setMinutes(0, 0, 0)); //Check that it is posted in this minute
							done();
						});
					});
			});
		});

		it("fails if user is not authorized", done => {
			requester.post(`${baseRoute}/${mealId}/signup`)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if meal doesn't exist", done => {
			requester.post(`${baseRoute}/${new ObjectId()}/signup`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error",
						"No meal with this ID or you can't signup for this meal");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if dorm doesn't exist", done => {
			requester.post(`/api/studenthome/${new ObjectId()}/meal/${mealId}/signup`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error",
						"Dorm doesn't exist!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if user was already added to dorm", done => {
			Dorm.findById(dormId).then(dorm => {
				dorm.meals
					.find(meal => meal._id.toString() == mealId.toString())
					.participants.push({ user: storedUser._id });
				return dorm.save();
			}).then(() => {
				requester.post(`${baseRoute}/${mealId}/signup`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error",
							"No meal with this ID or you can't signup for this meal");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						done();
					});
			});
		});

		it("fails if user was already added to dorm", done => {
			const tempUserIds = [];
			User.create({
				firstName: "FirstOtherTestName",
				lastName: "LastOtherTestName",
				studentNumber: "111111",
				emailAddress: "first@email.test",
				password: "TestPassword"
			}).then(user => {
				tempUserIds.push(user._id);
				return User.create({
					firstName: "FirstOtherOtherTestName",
					lastName: "LastOtherOtherTestName",
					studentNumber: "22222222",
					emailAddress: "second@email.test",
					password: "TestPassword"
				});
			}).then(user => {
				tempUserIds.push(user._id);
				return Dorm.findById(dormId);
			}).then(dorm => {
				tempUserIds.forEach(userId => dorm.meals
					.find(meal => meal._id.toString() == mealId.toString())
					.participants.push({ user: userId }));
				return dorm.save();
			}).then(() => {
				requester.post(`${baseRoute}/${mealId}/signup`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error",
							"No meal with this ID or you can't signup for this meal");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						done();
					});
			});
		});
	});

	describe("PUT to /api/studenthome/:dormId/meal/:mealId/signoff", () => {
		it("removes the currently logged in user as participant to the meal", done => {
			Dorm.findById(dormId).then(dorm => {
				dorm.meals
					.find(meal => meal._id.toString() == mealId.toString())
					.participants.push({ user: storedUser._id });
				return dorm.save();
			}).then(() => {
				requester.put(`${baseRoute}/${mealId}/signoff`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((_, res) => {
						Dorm.findById(dormId).then(dorm => 
							dorm.meals.find(meal => meal._id.toString() == mealId.toString())
						).then(meal => {
							expect(res).to.have.status(200);
							expect(meal._doc).to.haveOwnProperty("participants").and.to.be.instanceOf(Array).and.to.have.lengthOf(0);
							done();
						});
					});
			});
		});

		it("fails if user is not authorized", done => {
			requester.put(`${baseRoute}/${mealId}/signoff`)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if meal doesn't exist", done => {
			requester.put(`${baseRoute}/${new ObjectId()}/signoff`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error",
						"No meal with this ID or you can't signoff for this meal");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if dorm doesn't exist", done => {
			requester.put(`/api/studenthome/${new ObjectId()}/meal/${mealId}/signoff`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error",
						"Dorm doesn't exist!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if user was never added to meal", done => {
			requester.put(`${baseRoute}/${mealId}/signoff`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error",
						"No meal with this ID or you can't signoff for this meal");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	});
});
