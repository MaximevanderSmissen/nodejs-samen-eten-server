/* eslint-disable no-undef */
const chai = require("chai");
const expect = chai.expect;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const config = require("../../src/config/config");
const logger = config.logger;

const requester = require("../../requester");

//Models:
const User = require("../../src/models/user.model");
const Dorm = require("../../src/models/dorm.model");

const baseRoute = "/api";

describe("In testing the Default router, ", function() {
	this.retries(1);
	let mealId;
	let otherMealId;
	let participantId;
	let userId;
	let otherUserId;

	beforeEach(done => {
		User.create({
			firstName: "FirstTestName",
			lastName: "LastTestName",
			studentNumber: "0",
			emailAddress: "test@test.nl",
			password: bcrypt.hashSync("TestPassword", config.security.bcrypt.saltrounds) //Store hashed password
		}).then(user => {
			userId = user._id;
			return User.create({
				firstName: "FirstTestName",
				lastName: "LastTestName",
				studentNumber: "0",
				emailAddress: "test@test.nl",
				password: bcrypt.hashSync("TestPassword", config.security.bcrypt.saltrounds) //Store hashed password
			});
		}).then(user => {
			otherUserId = user._id;
			return Dorm.create({
				name: "TestDorm1",
				maker: userId,
				streetName: "Street1",
				houseNumber: "1",
				postalCode: "1000AA",
				cityName: "City1",
				phoneNumber: "088 4950964"
			});
		}).then(dorm => {
			dorm.meals.push({
				name: "Test Meal",
				maker: userId,
				postedOn: new Date(2020, 5, 3),
				offeredOn: new Date(2020, 5, 3),
				price: "0",
				allergyInfo: "Allergy Info",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 2
			});
			mealId = dorm.meals[0]._id;
			dorm.meals[0].participants.push({
				user: userId,
				postedOn: new Date(2020, 5, 6).getTime()
			});
			participantId = dorm.meals[0].participants[0]._id;
			dorm.meals[0].participants.push({
				user: otherUserId
			});
			dorm.meals.push({
				name: "Test Meal two",
				maker: userId,
				offeredOn: new Date(2021, 5, 3),
				price: "0",
				allergyInfo: "Allergy Info",
				ingredients: [ "Ingredient1" ],
				maxParticipants: 2
			});
			otherMealId = dorm.meals[1]._id;
			return dorm.save({ validateBeforeSave: true });
		}).then(() => {
			done();
		}).catch(error => {
			logger.error("Something went wrong!");
			logger.error(error);
		});
	});

	describe("a GET to /info ", () => {
		it("gets you the info about the app", done => {
			requester.get(`${baseRoute}/info`)
				.send()
				.end((_, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("author");
					expect(res.body.author).to.haveOwnProperty("name", "Maxime van der Smissen");
					expect(res.body.author).to.haveOwnProperty("studentnumber", 2143045);
					expect(res.body).to.haveOwnProperty("description", "Simple API server for the nodejs-samen-eten-server app");
					expect(res.body).to.haveOwnProperty("sonarQubeURL", "https://sonarqube.avans-informatica-breda.nl/dashboard?id=Nodejs-Samen-Eten-Server");
					done();
				});
		});
	});

	describe("a POST to /register ", () => {
		it("creates a new user", done => {
			const userProps = {
				firstName: "TestFirstname",
				lastName: "TestLastname",
				studentNumber: "2143045",
				emailAddress: "TestFirstname.TestLastname@student.avans.nl",
				password: "TestPassword"
			};
	
			User.countDocuments().then(oldCount => {
				requester.post(`${baseRoute}/register`)
					.send(userProps)
					.end((_, res) => {
						User.findOne({ emailAddress: "TestFirstname.TestLastname@student.avans.nl" })
							.then(user => {
								expect(res).to.have.status(201);
								expect(res.body).to.haveOwnProperty("token").and.to.be.a("string");
								const tokenPayload = jwt.verify(res.body.token, config.security.jwt.accessToken);
								expect(tokenPayload).to.haveOwnProperty("id", user._id.toString());
								expect(tokenPayload).to.haveOwnProperty("firstName", user.firstName);
								expect(tokenPayload).to.haveOwnProperty("lastName", user.lastName);
								expect(tokenPayload).to.haveOwnProperty("studentNumber", user.studentNumber.toString());
								expect(tokenPayload).to.haveOwnProperty("emailAddress", user.emailAddress);
								expect(tokenPayload).to.haveOwnProperty("iat").and.to.be.a("number");
								expect(tokenPayload).to.haveOwnProperty("exp").and.to.equal(tokenPayload.iat + 3600); // One hour after creation (iat)
								expect(res.body).to.haveOwnProperty("username", `${user.firstName} ${user.lastName}`);
								User.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount + 1);
									done();
								});
							});
					});
			});
		});

		it("without firstName fails", done => {
			const userProps = {
				lastName: "TestLastname",
				studentNumber: "2143045",
				emailAddress: "TestFirstname.TestLastname@student.avans.nl",
				password: "TestPassword"
			};
	
			User.countDocuments().then(oldCount => {
				requester.post(`${baseRoute}/register`)
					.send(userProps)
					.end((_, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
			});
		});

		it("without lastName fails", done => {
			const userProps = {
				firstName: "TestFirstname",
				studentNumber: "2143045",
				emailAddress: "TestFirstname.TestLastname@student.avans.nl",
				password: "TestPassword"
			};
	
			User.countDocuments().then(oldCount => {
				requester.post(`${baseRoute}/register`)
					.send(userProps)
					.end((_, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
			});
		});

		it("without studentNumber fails", done => {
			const userProps = {
				firstName: "TestFirstname",
				lastName: "TestLastname",
				emailAddress: "TestFirstname.TestLastname@student.avans.nl",
				password: "TestPassword"
			};
	
			User.countDocuments().then(oldCount => {
				requester.post(`${baseRoute}/register`)
					.send(userProps)
					.end((_, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
			});
		});

		it("without emailaddress fails", done => {
			const userProps = {
				firstName: "TestFirstname",
				lastName: "TestLastname",
				studentNumber: "2143045",
				password: "TestPassword"
			};
	
			User.countDocuments().then(oldCount => {
				requester.post(`${baseRoute}/register`)
					.send(userProps)
					.end((_, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
			});
		});

		it("without password fails", done => {
			const userProps = {
				firstName: "TestFirstname",
				lastName: "TestLastname",
				studentNumber: "2143045",
				emailAddress: "TestFirstname.TestLastname@student.avans.nl"
			};
	
			User.countDocuments().then(oldCount => {
				requester.post(`${baseRoute}/register`)
					.send(userProps)
					.end((_, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
			});
		});
	});

	describe("a POST to /login ", () => {
		it("logs a user in", done => {
			const userProps = {
				emailAddress: "test@test.nl",
				password: "TestPassword"
			};
	
			requester.post(`${baseRoute}/login`)
				.send(userProps)
				.end((_, res) => {
					User.findOne({ emailAddress: "test@test.nl" })
						.then(user => {
							expect(res).to.have.status(200);
							expect(res.body).to.haveOwnProperty("token").and.to.be.a("string");
							const tokenPayload = jwt.verify(res.body.token, config.security.jwt.accessToken);
							expect(tokenPayload).to.haveOwnProperty("id", user._id.toString());
							expect(tokenPayload).to.haveOwnProperty("firstName", user.firstName);
							expect(tokenPayload).to.haveOwnProperty("lastName", user.lastName);
							expect(tokenPayload).to.haveOwnProperty("studentNumber", user.studentNumber.toString());
							expect(tokenPayload).to.haveOwnProperty("emailAddress", user.emailAddress);
							expect(tokenPayload).to.haveOwnProperty("iat").and.to.be.a("number");
							expect(tokenPayload).to.haveOwnProperty("exp").and.to.equal(tokenPayload.iat + 3600); // One hour after creation (iat)
							expect(res.body).to.haveOwnProperty("username", `${user.firstName} ${user.lastName}`);
							done();
						});
				});
		});

		it("without emailaddress fails", done => {
			const userProps = {
				password: "TestPassword"
			};

			requester.post(`${baseRoute}/login`)
				.send(userProps)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("without password fails", done => {
			const userProps = {
				emailAddress: "test@test.nl"
			};

			requester.post(`${baseRoute}/login`)
				.send(userProps)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("with wrong emailAddress fails", done => {
			const userProps = {
				emailAddress: "wrong@email.nl",
				password: "TestPassword"
			};

			requester.post(`${baseRoute}/login`)
				.send(userProps)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("with wrong password fails", done => {
			const userProps = {
				emailAddress: "test@test.nl",
				password: "WrongPassword"
			};

			requester.post(`${baseRoute}/login`)
				.send(userProps)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("fails if jwt access token is unavailable", done => {
			const tempToken =  config.security.jwt.accessToken;
			config.security.jwt.accessToken = undefined;

			const userProps = {
				emailAddress: "test@test.nl",
				password: "TestPassword"
			};

			requester.post(`${baseRoute}/login`)
				.send(userProps)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					config.security.jwt.accessToken = tempToken;
					done();
				});
		});
	});

	describe("A GET to /meal/:mealId/participants", () => {
		it("returns an array of participants", done => {
			requester.get(`${baseRoute}/meal/${mealId}/participants`)
				.end((_, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.have.property("result").and.to.have.lengthOf(2);

					const participant = res.body.result.find(participant => participant._id == participantId.toString());

					expect(participant).to.haveOwnProperty("user", userId.toString());
					expect(participant).to.haveOwnProperty("postedOn", new Date(2020, 5, 6).getTime());
					done();
				});
		});

		it("returns an empty array if meal has no participants", done => {
			requester.get(`${baseRoute}/meal/${otherMealId}/participants`)
				.end((_, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.have.property("result").and.to.be.empty;
					done();
				});
		});

		it("fails if meal doesn't exist", done => {
			requester.get(`${baseRoute}/meal/${new Object()}/participants`)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.have.property("error", "Meal doesn't exist or something went wrong!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	});

	describe("A GET to /meal/:mealId/participants/:participantId", () => {
		it("returns an participant's details", done => {
			requester.get(`${baseRoute}/meal/${mealId}/participants/${participantId}`)
				.end((_, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("user");
					expect(res.body.user).to.haveOwnProperty("firstName", "FirstTestName");
					expect(res.body.user).to.haveOwnProperty("lastName", "LastTestName");
					expect(res.body.user).to.haveOwnProperty("studentNumber", "0");
					expect(res.body.user).to.haveOwnProperty("emailAddress", "test@test.nl");
					expect(res.body).to.haveOwnProperty("postedOn", new Date(2020, 5, 6).getTime());
					done();
				});
		});

		it("fails if participent doesn't exist", done => {
			requester.get(`${baseRoute}/meal/${mealId}/participants/${new Object()}`)
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.have.property("error", "Meal or participant doesn't exist or something went wrong!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	});

	describe("A request to a route that doesn't exist", () => {
		it("fails", done => {
			requester.get(`${baseRoute}/not/existing/route`)
				.end((_, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "No valid endpoint");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	});
});
