/* eslint-disable no-undef */
const mongoose = require("mongoose");
const logger = require("../src/config/config").logger;

before(done => {
	require("tracer").setLevel("warn");

	const mongoDBURL = process.env.MONGODB_CONNECTION_URL || "mongodb://localhost:27017/SamenEtenTestDatabase?retryWrites=true&w=majority";

	mongoose.connect(
		mongoDBURL,
		{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }
	);

	mongoose.connection
		.once("open", () => done())
		.on("error", err => {
			logger.error("Warning", err);
		});
});

beforeEach(done => {
	// eslint-disable-next-line no-unused-vars
	mongoose.connection.db.dropDatabase((error, _) => {
		if (error) logger.error(error);
		done();
	});
});

after(() => mongoose.connection.dropDatabase());
