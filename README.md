# Nodejs-Samen-Eten-Server

The Nodejs-Samen-Eten-Server is an API server driven by NodeJS intended for answering endpoints for the Samen-Eten-Server. This is a project as part of the Programming 4 course of Computer Science at Avans University of Applied Science.

## Installation

Use the node package manager (npm) to install this project after cloning or downloading it.

```bash
npm install
npm start
```

## Usage

You clone the project, and run npm install and npm start to install and start the server at a certain URL.
At that point, you can make calls to the /api endpoints and the server will respond.

## Contributing
No contributions will be allowed for this project

## License
[ISC](https://choosealicense.com/licenses/isc/)