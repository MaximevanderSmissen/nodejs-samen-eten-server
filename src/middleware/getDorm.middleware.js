const config = require("../config/config");
const Dorm = require("../models/dorm.model");

const logger = config.logger;
const title = "Middleware function:";

module.exports = (req, res, next) => {
	logger.info(title, "Get Dorm");
	Dorm.findById(req.params.dormId)
		.then(dorm => {
			if (!dorm) {
				return res.status(400).send({ error: "Dorm doesn't exist!", date: config.errorDate() });
			} else {
				req.body.dorm = dorm;
				next();
			}
		});
};