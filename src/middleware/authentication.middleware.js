const jwt = require("jsonwebtoken");
const config = require("../config/config");

const logger = config.logger;
const title = "Middleware function:";

module.exports = (req, res, next) => {
	logger.info(title, "Authentication");
	const authHeader = req.headers.authorization;
	const token = authHeader && authHeader.split(" ")[1];
	if (token == null) return res.status(401).send({ error: "User not authenticated!", date: config.errorDate() });

	jwt.verify(token, config.security.jwt.accessToken, (err, user) => {
		if (err) return res.status(401).send({ error: "User not authenticated!", date: config.errorDate() });
		req.body.user = user;
		next();
	});
};
