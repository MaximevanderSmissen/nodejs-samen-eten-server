const express = require("express");
const router = express.Router();

const config = require("../config/config");
const logger = config.logger;
const userService = require("../services/user.service");
const mealService = require("../services/meal.service");

router.use((req, _, next ) => {
	logger.info("API Route", req.originalUrl, req.method);
	next();
});

router.get("/info", (req, res) => res.status(200).send({
	author: {
		name: "Maxime van der Smissen",
		studentnumber: 2143045
	},
	description: "Simple API server for the nodejs-samen-eten-server app",
	sonarQubeURL: "https://sonarqube.avans-informatica-breda.nl/dashboard?id=Nodejs-Samen-Eten-Server"
}));

router.post("/register", (req, res) => userService.registerUser(req.body)
	.then(result => res.status(201).send(result))
	.catch(error => {
		logger.debug(error);
		res.status(401).send({ error: "User not authenticated!", date: config.errorDate() });
	}));

router.post("/login", (req, res) => userService.loginUser(req.body)
	.then(result => res.status(200).send(result))
	.catch(error => {
		logger.debug(error);
		res.status(401).send({ error: "User not authenticated!", date: config.errorDate() });
	}));

router.get("/meal/:mealId/participants", (req, res) => mealService.getParticipants(req.params.mealId)
	.then(participants => {
		const convertedParticipants = [];
		participants.forEach(participant => {
			const { postedOn, ...participantProps } = participant._doc;
			convertedParticipants.push({
				...participantProps,
				postedOn: new Date(postedOn).getTime()
			});
		});
		res.status(200).send({ result: convertedParticipants });
	})
	.catch(error => {
		logger.debug(error);
		res.status(400).send({ error: "Meal doesn't exist or something went wrong!", date: config.errorDate() });
	}));

router.get("/meal/:mealId/participants/:participantId",
	(req, res) => mealService.getParticipant(req.params.mealId, req.params.participantId)
		.then(participant => {
			const { postedOn, ...participantProps } = participant._doc;
			res.status(200).send({
				...participantProps,
				postedOn: new Date(postedOn).getTime()
			});
		})
		.catch(error => {
			logger.debug(error);
			res.status(400).send({
				error: "Meal or participant doesn't exist or something went wrong!",
				date: config.errorDate()
			});
		}));

module.exports = router;
