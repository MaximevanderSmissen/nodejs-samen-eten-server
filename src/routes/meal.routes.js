const express = require("express");
const router = express.Router();

const mealService = require("../services/meal.service");
const authorization = require("../middleware/authentication.middleware");

const config = require("../config/config");
const logger = config.logger;
const title = "API Route";

const response = (meal, res, method) => {
	try {
		const { postedOn, offeredOn, ...mealProps } = meal._doc;
		res.status(200).send({
			postedOn: new Date(postedOn).getTime(),
			offeredOn: new Date(offeredOn).getTime(),
			...mealProps
		});
	} catch (error) {
		logger.debug(error);
		res.status(400).send({ error: `No meal with this ID or you are not authorized to ${method} this meal`, date: config.errorDate() });
	}
};

router.use((req, _, next ) => {
	logger.info(title, req.originalUrl, req.method);
	next();
});

router.post("/", authorization,
	(req, res) => mealService.createMeal(req.body)
		.then((meal) =>
		{
			const { postedOn, offeredOn, ...mealProps } = meal._doc;
			res.status(201).send({
				postedOn: new Date(postedOn).getTime(),
				offeredOn: new Date(offeredOn).getTime(),
				...mealProps
			});
		})
		.catch(error => res.status(400).send({ error: error.message, date: config.errorDate() })));

router.get("/", (req, res) => mealService.getMeals(req.body)
	.then(dorm => {
		const meals = [];
		dorm.meals.forEach(meal => {
			const { offeredOn, ...mealProps } = meal._doc;
			meals.push({
				...mealProps,
				offeredOn: new Date(offeredOn).getTime()
			});
		});
		res.status(200).send({ results: meals });
	})
	.catch(error => res.status(400).send({ error: error.message, date: config.errorDate() })));

router.get("/:mealId", (req, res) => mealService.getMeal(req.body.dorm, req.params.mealId)
	.then(dorm => response(dorm, res, "get"))
	.catch(error => res.status(400).send({ error: error.message, date: config.errorDate() })));

router.put("/:mealId", authorization,
	(req, res) => mealService.updateMeal(req.params.mealId, req.body)
		.then(meal => response(meal, res, "update"))
		.catch(error => res.status(400).send({ error: error.message, date: config.errorDate() })));

router.delete("/:mealId", authorization,
	(req, res) => mealService.deleteMeal(req.params.mealId, req.body)
		.then(dorm => response(dorm, res, "delete"))
		.catch(error => res.status(400).send({ error: error.message, date: config.errorDate() })));

router.post("/:mealId/signup", authorization,
	(req, res) => mealService.signUp(req.params.mealId, req.body)
		.then(() => res.sendStatus(201))
		.catch(() => res.status(400).send({ error: "No meal with this ID or you can't signup for this meal", date: config.errorDate() })));

router.put("/:mealId/signoff", authorization,
	(req, res) => mealService.signOff(req.params.mealId, req.body)
		.then(() => res.sendStatus(200))
		.catch(() => res.status(400).send({ error: "No meal with this ID or you can't signoff for this meal", date: config.errorDate() })));

module.exports = router;
