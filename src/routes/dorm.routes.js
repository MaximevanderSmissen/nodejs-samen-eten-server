const express = require("express");
const router = express.Router();

const dormService = require("../services/dorm.service");
const authorization = require("../middleware/authentication.middleware");
const getDorm = require("../middleware/getDorm.middleware");

const mealRoutes = require("./meal.routes");

const config = require("../config/config");
const logger = config.logger;
const title = "API Route";

const response = (dorm, res, method) => {
	try {
		dorm._id;
		res.status(200).send(dorm);
	} catch (error) {
		logger.debug(error);
		res.status(400).send({ error: `No dorm with this ID or you are not authorized to ${method} this dorm`, date: config.errorDate() });
	}
};

router.use((req, _, next ) => {
	logger.info(title, req.originalUrl, req.method);
	next();
});

router.post("/", authorization,
	(req, res) => dormService.createDorm(req.body)
		.then(dorm => res.status(201).send(dorm))
		.catch(error => res.status(400).send({ error: error.message, date: config.errorDate() })));

router.get("/", (req, res) => dormService.getDorms(req.query.name, req.query.city)
	.then(dorms => res.status(200).send({ results: dorms }))
	.catch(error => res.status(400).send({ error: error.message, date: config.errorDate() })));

router.get("/:dormId", (req, res) => dormService.getDorm(req.params.dormId)
	.then(dorm => response(dorm, res, "get"))
	.catch(error => res.status(400).send({ error: error.message, date: config.errorDate() })));

router.put("/:dormId", authorization,
	(req, res) => dormService.updateDorm({ _id: req.params.dormId, maker: req.body.user.id }, req.body)
		.then(dorm => response(dorm, res, "update"))
		.catch(error => res.status(400).send({ error: error.message, date: config.errorDate() })));

router.put("/:dormId/user", authorization,
	(req, res) => dormService.addUserToDorm(req.params.dormId, req.body.user, req.body.userId)
		.then(dorm => res.status(200).send(dorm))
		.catch(error => res.status(400).send({ error: error.message,
			date: config.errorDate() })));

router.delete("/:dormId", authorization,
	(req, res) => dormService.deleteDorm({ _id: req.params.dormId, maker: req.body.user.id })
		.then(dorm => response(dorm, res, "delete"))
		.catch(error => res.status(400).send({ error: error.message, date: config.errorDate() })));

router.use("/:dormId/meal", getDorm, mealRoutes);

module.exports = router;
