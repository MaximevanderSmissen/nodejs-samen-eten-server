const express = require("express");
const app = express();
const config = require("./config/config");

const bodyParser = require("body-parser");
app.use(bodyParser.json());

// Add CORS headers
app.use(function (req, res, next) {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
	res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type");
	res.setHeader("Access-Control-Allow-Credentials", true);
	next();
});

const defaultRoutes = require("./routes/miscellaneous.routes");
const dormRoutes = require("./routes/dorm.routes");

app.use("/api", defaultRoutes);
app.use("/api/studenthome", dormRoutes);

// Unknow endpoints
app.all("*", (_, res) =>
	res.status(404).send({
		error: "No valid endpoint",
		date: config.errorDate()
	}));

module.exports = app;
