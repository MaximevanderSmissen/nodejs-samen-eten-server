const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ParticipantSchema = require("./participant.model");

const MealSchema = new Schema({
	name: {
		type: String,
		required: [true, "Meal name is required!"]
	},
	maker: {
		type: Schema.Types.ObjectId,
		ref: "user",
		required: [true, "Meal maker is required!"]
	},
	postedOn: {
		type: Date,
		default: new Date()
	},
	offeredOn: {
		type: Date,
		required: [true, "Meal offered date is required!"]
	},
	price: {
		type: String,
		validate: {
			validator: price => parseFloat(price) >= 0,
			message: "Meal price must be positive!"
		},
		required: [true, "Meal price is required!"]
	},
	allergyInfo: {
		type: String,
		required: [true, "Meal allergy information is required!"]
	},
	ingredients: {
		type: [String],
		validate: {
			validator: ingredients => ingredients && ingredients.length > 0,
			message: "Meal ingredients must be specified and may not be empty!"
		}
	},
	maxParticipants: {
		type: Number,
		validate: {
			validator: maxParticipants => maxParticipants > 0,
			message: "Meal maximum number of participants must be greater than zero!"
		},
		required: [true, "Meal maximum number of participants is required!"]
	},
	participants: {
		type: [ParticipantSchema],
		default: []
	}
});

MealSchema.post("validate", function(_, next) {
	if (this.participants.length > this.maxParticipants) throw Error("Too many participants!");
	else {
		const foundIds = [];
		this.participants.forEach(participant => {
			if (foundIds.includes(participant.user.toString())) {
				throw Error("No duplicate users allowed");
			} else {
				foundIds.push(participant.user.toString());
			}
		});
		next();
	}
});

module.exports = MealSchema;
