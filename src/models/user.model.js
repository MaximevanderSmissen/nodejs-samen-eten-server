const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	firstName: {
		type: String,
		required: [true, "First name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required!"]
	},
	studentNumber: {
		type: String,
		required: [true, "Student number is required!"]
	},
	emailAddress: {
		type: String,
		required: [true, "Email address is required!"],
		unique: true
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	}
});

const User = mongoose.model("user", UserSchema);

module.exports = User;
