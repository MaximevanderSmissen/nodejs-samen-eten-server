const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ParticipantSchema = new Schema({
	user: {
		type: Schema.Types.ObjectId,
		ref: "user",
		required: [true, "User is required!"]
	},
	postedOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = ParticipantSchema;
