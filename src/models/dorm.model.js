const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MealSchema = require("./meal.model");

const DormSchema = new Schema({
	name: {
		type: String,
		required: [true, "Dorm name is required!"]
	},
	maker: {
		type: Schema.Types.ObjectId,
		ref: "user",
		required: [true, "Dorm maker is required!"]
	},
	streetName: {
		type: String,
		required: [true, "Street name is required!"]
	},
	houseNumber: {
		type: String,
		required: [true, "House number is required!"]
	},
	postalCode: {
		type: String,
		required: [true, "Postal code is required!"],
		match: /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i
	},
	cityName: {
		type: String,
		required: [true, "City name is required!"]
	},
	phoneNumber: {
		type: String,
		required: [true, "Phone number is required!"],
		match: /^([0]{1}[6]{1}[-\s]*[1-9]{1}[\s]*([0-9]{1}[\s]*){7})|([0]{1}[1-9]{1}[0-9]{1}[0-9]{1}[-\s]*[1-9]{1}[\s]*([0-9]{1}[\s]*){5})|([0]{1}[1-9]{1}[0-9]{1}[-\s]*[1-9]{1}[\s]*([0-9]{1}[\s]*){6})$/
	},
	students: [{
		type: Schema.Types.ObjectId,
		ref: "user"
	}],
	meals: {
		type: [MealSchema],
		default: []
	}
});

DormSchema.pre("validate", function(next) {
	const foundIds = [];
	this.students.forEach(userId => {
		if (foundIds.includes(userId.toString())) {
			throw Error("No duplicate students allowed!");
		} else {
			foundIds.push(userId.toString());
		}
	});
	next();
});

DormSchema.index({
	streetName: 1,
	houseNumber: 1,
	postalCode: 1,
	cityName: 1
}, {
	unique: true
});

const Dorm = mongoose.model("dorm", DormSchema);

module.exports = Dorm;
