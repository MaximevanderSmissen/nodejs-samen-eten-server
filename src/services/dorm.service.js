const Dorm = require("../models/dorm.model");
const User = require("../models/user.model");
const config = require("../config/config");
require("../models/user.model");

const logger = config.logger;
const title = "Service function:";

module.exports = {
	createDorm: ({ user, ...dormProps }) => {
		logger.info(title, "Create Dorm");
		dormProps.maker = user.id;
		return Dorm.create(dormProps);
	},
	getDorms: (dormName, cityName) => {
		logger.info(title, "Get Dorms");
		let filter = {};
		if (dormName) filter.name = new RegExp(`^${dormName}`);
		if (cityName) filter.cityName = cityName;
		return Dorm.find(filter).select(config.dormListProjection);
	},
	getDorm: (dormId) => {
		logger.info(title, "Get Dorm Details");
		return Dorm.findById(dormId).select(config.dormDetailsProjection)
			.populate("maker", config.userProjection)
			.populate("students", config.userProjection);
	},
	// eslint-disable-next-line no-unused-vars
	updateDorm: (where, { maker, _id, ...dormProps }) => {
		logger.info(title, "Update Dorm");
		return Dorm.findOneAndUpdate(where, dormProps, { upsert: false, new: true, runValidators: true })
			.select(config.dormDetailsProjection);
	},
	deleteDorm: (where) => {
		logger.info(title, "Delete Dorm");
		return Dorm.findOneAndDelete(where).select(config.dormDetailsProjection);
	},
	addUserToDorm: (dormId, user, userIdToAdd) => {
		logger.info(title, "Add user to Dorm");
		if (!userIdToAdd) userIdToAdd = user.id;
		return User.findById(userIdToAdd).select(config.userProjection).orFail(new Error("No user on given id!"))
			.then(() => Dorm.findOne({ "_id": dormId, maker: user.id }))
			.then(dorm => {
				if (dorm == null) throw Error("No Dorm found!");
				else {
					dorm.students.push(userIdToAdd);
					return dorm.save({ validateBeforeSave: true });
				}
			});
	}
};
