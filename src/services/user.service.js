const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("../config/config");

const logger = config.logger;
const title = "Service function:";

const service = {};
service.test = "test";
service.signToken = (user) => {
	logger.info(title, "Sign Token");
	const token = jwt.sign({
		id: user._id,
		firstName: user.firstName,
		lastName: user.lastName,
		studentNumber: user.studentNumber,
		emailAddress: user.emailAddress
	}, config.security.jwt.accessToken, { expiresIn: "1h" });
	return { token, username: `${user.firstName} ${user.lastName}` };
};
service.registerUser = ({ password, ...userProps }) => {
	logger.info(title, "Register User");
	return bcrypt.hash(password, config.security.bcrypt.saltrounds)
		.then(hashedPassword => User.create({ ...userProps, password: hashedPassword }))
		.then(user => service.signToken(user));
};
service.loginUser = ({ emailAddress, password }) => {
	logger.info(title, "Login User");
	return User.findOne({ emailAddress }, { __v: 0 })
		.then((user) => bcrypt.compare(password, user.password)
			.then(result => {
				if (result) {
					return service.signToken(user);
				} else {
					throw Error("Incorrect Password!");
				}
			}));
};

module.exports = service;
