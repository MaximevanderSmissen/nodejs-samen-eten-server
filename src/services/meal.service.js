const ObjectId = require("mongoose").Types.ObjectId;
const Dorm = require("../models/dorm.model");
const config = require("../config/config");
require("../models/user.model");

const logger = config.logger;
const title = "Service function:";

module.exports = {
	createMeal: ({ user, dorm, offeredOn, ...mealProps }) => {
		logger.info(title, "Create Meal");
		if (
			!dorm.maker.equals(ObjectId(user.id))
			&& !dorm.students.includes(ObjectId(user.id))
		) return Promise.reject(Error("You must be a student of the dorm in order to make a meal for it!"));
		else {
			dorm.meals.push({ ...mealProps, maker: user.id, offeredOn: new Date(offeredOn) });
			return dorm.save({ validateBeforeSave: true })
				.then(dorm => dorm.meals[dorm.meals.length - 1]);
		}
	},
	getMeals: ({ dorm }) => {
		logger.info(title, "Get Meals");
		return Dorm.findById(dorm._id)
			.select({
				"meals._id": 1,
				"meals.name": 1,
				"meals.maker": 1,
				"meals.offeredOn": 1,
				"meals.price": 1
			})
			.populate("meals.maker", config.userProjection);
	},
	getMeal: (dorm, mealId) => {
		logger.info(title, "Get Meal Details");
		return dorm.populate("meals.maker", config.userProjection).execPopulate()
			.then(populatedDorm => populatedDorm.meals.id(mealId));
	},
	// eslint-disable-next-line no-unused-vars
	updateMeal: (mealId, { dorm, user, maker, _id, ...mealProps }) => {
		logger.info(title, "Update Meal");
		const meal = dorm.meals.id(mealId);
		if (meal && meal.maker == user.id) {
			Object.assign(meal, mealProps);
			return Dorm.findOneAndUpdate(
				{ _id: dorm._id, "meals._id": mealId, "meals.maker": user.id },
				{ "$set": { "meals.$": meal } },
				{ upsert: false, new: true, runValidators: true }
			).then(dorm => {
				return dorm.meals.id(mealId);
			});
		} else {
			// eslint-disable-next-line no-unused-vars
			return new Promise((resolve, _) => resolve(null));
		}
	},
	deleteMeal: (mealId, { dorm, user }) => {
		logger.info(title, "Delete Meal");
		const meal = dorm.meals.id(mealId);
		if (meal && meal.maker == user.id) {
			meal.remove();
			return dorm.save({ validateBeforeSave: true }).then(() => meal);
		} else {
			// eslint-disable-next-line no-unused-vars
			return new Promise((resolve, _) => resolve(null));
		}
	},
	signUp: (mealId, { dorm, user }) => {
		logger.info(title, "Signup for Meal");
		const meal = dorm.meals.id(mealId);
		if (meal) {
			meal.participants.push({ user: user.id });
			return dorm.save({ validateBeforeSave: true });
		} else {
			// eslint-disable-next-line no-unused-vars
			return new Promise((_, reject) => reject(null));
		}
	},
	signOff: (mealId, { dorm, user }) => {
		logger.info(title, "SignOff for Meal");
		const meal = dorm.meals.id(mealId);
		if (meal && meal.participants.find(participant => participant.user == user.id)) {
			return Dorm.updateOne(
				{ _id: dorm._id, "meals._id": mealId },
				{ $pull: { "meals.$.participants": { user: user.id } } },
				{ upsert: false, new: true, runValidators: true }
			);
		} else {
			// eslint-disable-next-line no-unused-vars
			return new Promise((_, reject) => reject(null));
		}
	},
	getParticipants: mealId => {
		logger.info(title, "Get participants of Meal");
		return Dorm.findOne({ "meals._id": mealId }, { "meals.$": 1 })
			.then(dorm => dorm.meals[0].participants);
	},
	getParticipant: async (mealId, participantId) => {
		logger.info(title, "Get participant of Meal");
		return Dorm.findOne({ "meals._id": mealId }, { "meals.$": 1 })
			.populate("meals.participants.user", config.userProjection)
			.then(dorm => {
				return dorm.meals.id(mealId).participants.id(participantId);
			});
	}
};
